::------- ------------------创建cocos2dx工程脚本 ---------------------------------------
::---功能: 创建cocos2dx工程，基于cocos2dx 3.x 版本
::--------------------------------------------------------------------------------------
::---注意：此脚本运行前需设置COCOS2DX_DIR、PROJECT_DIR变量值，设置为cocos2dx和工程保存目录
::-------  正确设置安装python2.7.6+、NDK r8e+、ANT、Android SDK、JDK以及他们的环境变量设置
::-------  此脚本可以放置在任何目录下
::--------------------------------------------------------------------------------------
::---使用：按照提示分别输入项目名称与包名称即可，
::--------脚本执行完成后，会在指定路径下创建工程
::--------------------------------------------------------------------------------------
::---创建时间：2014.5.18 ---作者：酷爱小铅 ------修改时间：2015.6.19 --- 修改：卢俊-----
::--------------------------------------------------------------------------------------



@echo off
:: ------设置“必备”常数变量 (注意反斜杠方向)-----
set COCOS2DX_DIR=F:\code\cocos2dx\cocos2d-x-3.4
set PROJECT_DIR=F:\code\cocos2dx\projects


:: -----提示输入项目名称与包名称
set /p projectName=请输入项目名称:
if "%projectName%"=="" goto inputError  
set /p packageName=请输入包标示名:
if "%packageName%"=="" goto inputError


::删除临时脚本文件
if exist "creat_tmp.bat" del "creat_tmp.bat" /f/q

::创建临时脚本文件
::echo >creat_tmp.bat

::添加创建工程命令
echo @echo off>>creat_tmp.bat
echo cocos new %projectName% -p %packageName% -l cpp -d %PROJECT_DIR%>>creat_tmp.bat

::执行创建工程
call creat_tmp.bat

::复制编译脚本
copy cc2dx_proj_complie_3.x.bat %PROJECT_DIR%\%projectName%\cc2dx_proj_complie_3.x.bat

::删除临时脚本文件
if exist "creat_tmp.bat" del "creat_tmp.bat" /f/q

echo 创建成功................
pause
exit

:inputError
@echo 项目名称或者包标示名不能为空！  
@pause 