::--------      脚本执行      --------
::------------------------------------
::---创建时间：2015.7.23 --- 卢俊-----
::------------------------------------



@echo off


echo 编译平台：1-win32,2-Android

set /p platformNo=请选择输入平台编号:
if "%platformNo%"=="" goto inputError
if "%platformNo%"=="1" goto win32
if "%platformNo%"=="2" goto android


:win32
cocos run -p win32

pause  
exit  


:android

::python proj.android/build_native.py
cocos run -p android

pause  
exit  

:inputError  
@echo 输入平台编号不能为空！  
pause 