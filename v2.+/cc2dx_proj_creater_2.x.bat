::------- ------------------创建cocos2dx工程脚本 ---------------------------------------
::---功能: 创建cocos2dx工程，基于cocos2dx 2.2.3 版本
::--------------------------------------------------------------------------------------
::---注意：此脚本运行前需设置COCOS2DX_DIR、NDK_DIR 变量值，设置为cocos2dx与ndk的目录
::------- 还需正确设置安装python2.7.6 以及将python路径加入到PATH环境变量中
::------- 此脚本可以放置在任何目录下
::--------------------------------------------------------------------------------------
::---使用：按照提示分别输入项目名称与包名称即可，
::--------脚本执行完成后，会在桌面创建到工程目录的快捷方式
::--------------------------------------------------------------------------------------
::---创建时间：2014.5.18------最后修改时间：2014.5.18 ---作者：酷爱小铅 ----------------
::--------------------------------------------------------------------------------------



@echo off
:: ------设置“必备”常数变量 (注意反斜杠方向)-----
set COCOS2DX_DIR=F:\code\cocos2dx\cocos2d-x-2.2.3
set NDK_DIR=F:\code\ndk\android-ndk-r8b


:: -----提示输入项目名称与包名称
set /p projectName=请输入项目名称:
if "%projectName%"=="" goto inputError  
set /p packageName=请输入包标示名:
if "%packageName%"=="" goto inputError  


::-------创建工程，默认在cocos2dx/projects文件夹下
cd /d %COCOS2DX_DIR%/tools/project-creator
create_project.py -project %projectName% -package %packageName% -language cpp   

::------修改默认的项目设置----
echo 正在修改项目相关设置..... 

set NDK_MODULE_PATH= %COCOS2DX_DIR%;%COCOS2DX_DIR%/cocos2dx/platform/third_party/android/prebuilt
echo NDK_MODULE_PATH:=%NDK_MODULE_PATH%>>%COCOS2DX_DIR%/projects/%projectName%/proj.android/jni/Application.mk

::----创建桌面快捷方式 ----
::echo [InternetShortcut] >"%USERPROFILE%\Desktop\%projectName%.url"
::echo URL="%COCOS2DX_DIR%\projects\%projectName%" >>"%USERPROFILE%\Desktop\%projectName%.url"
::echo IconIndex=0 >>"%USERPROFILE%\Desktop\%projectName%.url"
::echo IconFile="%COCOS2DX_DIR%\template\multi-platform-js\proj.win32\res\game.ico" >>"%USERPROFILE%\Desktop\%projectName%.url"

::----创建快捷方式 ----
echo [InternetShortcut] >"F:\code\cocos2dx\projects\%projectName%.url"
echo URL="%COCOS2DX_DIR%\projects\%projectName%" >>"F:\Code\cocos2dx\projects\%projectName%.url"
echo IconIndex=0 >>"F:\code\cocos2dx\projects\%projectName%.url"
echo IconFile="%COCOS2DX_DIR%\template\multi-platform-js\proj.win32\res\game.ico" >>"F:\code\cocos2dx\projects\%projectName%.url"


echo 成功................
::-------退出与出错处理-------
pause  
exit  

:inputError  
@echo 项目名称或者包标示名不能为空！  
pause 